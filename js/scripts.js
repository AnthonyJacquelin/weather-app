$(document).ready(function() {

    /** INIT */
    const apiUrl = 'https://api-adresse.data.gouv.fr/search/?q=';
    const format = '&type=municipality';
    const windowWidth = window.screen.width;

    if (windowWidth >= 1200) {
        $('body').addClass('ui_desktop');
    }
    if (windowWidth <= 768) {
        $('body').addClass('ui_mobile');
    }

    /** SEARCH **/
    $('#search_container #search').on('input', function() {

        let value = $(this).val();
        let url = apiUrl + value + format;
        $('.search_container-results-item').remove();
        $('#search .fa-times-circle').css('opacity', 1);

        $.getJSON(url, function(json) {
            //console.log(json);
            $.each(json.features, function(i, obj) {
                /** ITEM OPTIONS */
                let city = obj.properties.city;
                var postalCodeContext1 = obj.properties.context.toString().split(',').slice(1);
                var postalCodeContext2 = obj.properties.context.toString().split(',').slice(2);
                let postalCode = obj.properties.postcode;
                let postalCodeVideo = postalCode.toString().substring(0, 2);
                let department = obj.properties.context.toString().split(',').slice(1);
                let population = obj.properties.population;
                let coordinates = obj.geometry.coordinates;
                coordinates.toString().split(",");
                let coordinatesLatitude = coordinates[1];
                let coordinatesLongitude = coordinates[0];
                $('<div class="search_container-results-item" data-population=' + population + ' data-postalCodeContext1=' + postalCodeContext1 + ' data-postalCodeContext2=' + postalCodeContext2 + ' data-postalCode=' + postalCode + ' data-postalCodeVideo=' + postalCodeVideo + ' data-coordinatesLatitude=' + coordinatesLatitude + ' data-coordinatesLongitude=' + coordinatesLongitude + '><span>' + city + '</span><b>' + postalCode + department + '</b></div>').appendTo('#search_container-results');
            });

        });
    });

    /** CLICK ON SEARCH ITEM*/
    $('body').on('click', '.search_container-results-item', function() {

        let cityName = $(this).find('span').text();
        let url = '/cities/' + cityName + '.php';

        //INIT
        let coordinatesLatitude = $(this).attr('data-coordinatesLatitude');
        let coordinatesLongitude = $(this).attr('data-coordinatesLongitude');
        cityName.toString().split(",");
        let population = $(this).attr('data-population');
        let postalCode = $(this).attr('data-postalCode');
        let postalCodeContext1 = $(this).attr('data-postalCodeContext1');
        let postalCodeContext2 = $(this).attr('data-postalCodeContext2');

        $('<h1>' + cityName + '</h1>').appendTo('#page_container .page_container-header');
        $('<h2>' + postalCode + ', ' + postalCodeContext1 + ' ' + postalCodeContext2 + '</h2>').appendTo('#page_container .page_container-header');
        $('<h5>Lat. ' + coordinatesLatitude + ' Long. ' + coordinatesLongitude + '</h5>').appendTo('#page_container .page_container-header');

        postalCode = postalCode.slice(0, 2);

        if (postalCode != "33" && postalCode != "59" && postalCode != "62" && postalCode != "80" && postalCode != "76" && postalCode != "27" && postalCode != "17" && postalCode != "50" && postalCode != "35" && postalCode != "22" && postalCode != "29" && postalCode != "56" && postalCode != "44" && postalCode != "85" && postalCode != "17" && postalCode != "33" && postalCode != "40" && postalCode != "64" && postalCode != "66" && postalCode != "11" && postalCode != "34" && postalCode != "30" && postalCode != "13" && postalCode != "83" && postalCode != "06") {
            $('.page_container-details').remove();
        } else {
            //TIDES
            let urlTides = "https://tides.p.rapidapi.com/tides?radius=200&latitude=" + coordinatesLatitude + "&longitude=" + coordinatesLongitude;
            urlTides.toString();
            var tideSettings = {
                "async": true,
                "crossDomain": true,
                "url": urlTides,
                "method": "GET",
                "headers": {
                    "x-rapidapi-host": "tides.p.rapidapi.com",
                    "x-rapidapi-key": "9442aad5a3msh59ebcfbd3a08359p1302cbjsnd7a57f6d8f0d"
                }
            }
            $.ajax(tideSettings).done(function(response) {
                $.each(response.extremes, function(i, obj) {
                    console.log(obj);
                    let datetimeHours = obj.datetime.toString().split('T').slice(1).toString().split('+').slice(0).toString().split(',')[0].substring(0, 2).toString();
                    let datetimeMinutes = obj.datetime.toString().split('T').slice(1).toString().split('+').slice(0).toString().split(',')[0].substring(3, 5).toString();
                    if (datetimeHours == 23) {
                        datetimeHours = 0;
                    } else {
                        datetimeHours++;
                    }
                    if (datetimeHours == 23) {
                        datetimeHours = 0;
                    } else {
                        datetimeHours++;
                    }
                    let state = obj.state;
                    if (state == 'HIGH TIDE') {
                        state = 'Haute';
                        $('<h4 class="page_container-details-tide page_container-details-tide_high"><b>' + datetimeHours + 'h' + datetimeMinutes + '</b><span>' + state + '</span></h4>').appendTo('#page_container .page_container-details');

                    } else {
                        state = 'Basse';
                        $('<h4 class="page_container-details-tide page_container-details-tide_low"><b>' + datetimeHours + 'h' + datetimeMinutes + '</b><span>' + state + '</span></h4>').appendTo('#page_container .page_container-details');
                    }
                });
            });
        }

        //WEATHER
        var weatherSettings = {
            "async": true,
            "crossDomain": true,
            "url": 'https://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&appid=d9b111b0a4dfbc39333a7c680a49e754&units=metric&lang=fr',
            "method": "GET"
        }
        $.ajax(weatherSettings).done(function(response) {
            console.log(response);

            let sunrise = response.sys.sunrise;
            let sunset = response.sys.sunset;

            let unix_timestamp = sunrise;
            var date = new Date(unix_timestamp * 1000);
            var hours = date.getHours();
            var minutes = "0" + date.getMinutes();
            var formattedTime = hours + 'h' + minutes.substr(-2);
            $('<h3>Lever soleil</h3>').appendTo('.page_container-contain .data-sunrise');
            $('<h4>' + formattedTime + '</h4>').appendTo('.page_container-contain .data-sunrise');

            unix_timestamp = sunset;
            date = new Date(unix_timestamp * 1000);
            hours = date.getHours();
            minutes = "0" + date.getMinutes();
            seconds = "0" + date.getSeconds();
            formattedTime = hours + 'h' + minutes.substr(-2);
            $('<h3>Coucher soleil</h3>').appendTo('.page_container-contain .data-sunset');
            $('<h4>' + formattedTime + '</h4>').appendTo('.page_container-contain .data-sunset');


            let temperature = response.main.temp;
            let humidity = response.main.humidity;
            let description = response.weather[0].description;
            let wind = response.wind.speed;
            var convert = (function() {
                var conversions = {
                    speed: {
                        ms: 1, // use m/s as our base unit
                        kmh: 3.6,
                        mph: 2.23693629,
                        knots: 1.94384449
                    },

                    distance: {
                        m: 1, // use meters as our base
                        inches: 39.3700787402, // can't use "in" as that's a keyword. Darn.
                        ft: 3.280839895,
                        mi: 0.000621371192,
                        nm: 0.000539956803 // nautical miles, not nanometers
                    },

                    mass: {
                        g: 1, // use grams as our base
                        lb: 0.002204622622,
                        oz: 0.0352739619
                    }
                };

                function Unit(type, unit, base) {
                    this.value = base * conversions[type][unit];
                    this.to = {};
                    for (var otherUnit in conversions[type]) {
                        (function(target) {
                            this.to[target] = function() {
                                return new Unit(type, target, base);
                            }
                        }).call(this, otherUnit);
                    }
                }

                Unit.prototype = {
                    yield: function() {
                        return this.valueOf();
                    },

                    toString: function() {
                        return String(this.value);
                    },

                    valueOf: function() {
                        return this.value;
                    }
                };

                var types = {};
                for (var type in conversions) {
                    (function(type) {
                        types[type] = function(value) {
                            var units = {};
                            for (var unit in conversions[type]) {
                                (function(unit) {
                                    units[unit] = function() {
                                        return new Unit(type, unit, value / conversions[type][unit]);
                                    }
                                }(unit));
                            }
                            return units;
                        };
                    }(type));
                }

                return types;
            }());
            let windKmh = convert.speed(wind).mph().to.kmh();
            $('<h4><b>' + description + '</b>' + Math.round(temperature) + ' °C</h4>').appendTo('.page_container-contain .data-temperature');
            $('<div><h5>Humidité : ' + humidity + ' %</h5><h5>Vent : ' + Math.round(windKmh) + ' km/h</h5></div>').appendTo('.page_container-contain .data-temperature');
        });

        $.ajax({
            url: 'https://meteo-maree.info' + url,
            type: 'HEAD',
            error: function() {
                $('body').toggleClass('page_open');
            },
            success: function() {
                location.href = 'https://meteo-maree.info' + url;
            }
        });

    });

    /** INIT SEARCH */
    $('.search_open').click(function() {
        $('body').toggleClass('search_open');
        $('html, body').animate({
            scrollTop: 0
        }, 300);
        $('#search input').focus();
    });
    $('#search_container #search .fa-chevron-left').click(function() {
        $('body').toggleClass('search_open');
    });
    $('#search_container #search .fa-times-circle').click(function() {
        $('#search_container #search').val('');
        $('.search_container-results-item').remove();
        $('#search .fa-times-circle').css('opacity', 0);
    });
    $('#navbar-back_home').click(function() {
        $(".page_container-header h1, .page_container-header h2, .page_container-header h5, .page_container-contain-item h4,  .page_container-contain-item div, .page_container-details-tide, .page_container-contain-item h3").remove();
        $('body').toggleClass('page_open');
    });

    /** SIDEBAR **/
    $('#navbar-icon').click(function() {
        $('body').toggleClass('navbar_open');
    });

    /** POP UP **/
    $('#background_pop_up').click(function() {
        $('body').removeClass('navbar_open');
    });

    /** SCROLL **/
    $(window).scroll(function() {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 0) {
            if ($('body').hasClass("scroll_on")) {
                return false;
            } else {
                $('body').addClass('scroll_on');
            }
        } else {
            $('body').removeClass('scroll_on');
        }
    });




});