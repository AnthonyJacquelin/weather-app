<!DOCTYPE html>
<html lang="FR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Klima : Meteo, maree et infos</title>
    <meta name="description" content="L'essentiel des informations sur les marées, hauteur et coefficient. Prédictions et calendrier météo partout en France, basés les données officielles.">
    <meta name="keywords" content="klima clima climat maree ports annuaire calendrier horaire marée marre heure hauteur coefficient météo meteo temps temperature température france paris">
    <meta name="Content-Language" content="fr">
    <meta name="Subject" content="Marées et météo partout en France">
    <meta name="Copyright" content="Klima">
    <meta name="Author" content="Klima">
    <link rel="shortcut icon" type="image/x-icon" href="img/logotype.png">

    <!-- TWITTER -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@Meteo-maree.info">
    <meta name="twitter:creator" content="@Meteo-maree.info">

    <!-- FACEBOOK -->
    <meta property="og:title" content="Klima : Meteo, maree et infos">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://meteo-maree.info/">
    <meta property="og:image" content="https://meteo-maree.info/img/social_network.png">
    <meta property="og:description" content="L'essentiel des informations sur les marées, hauteur et coefficient. Prédictions et calendrier météo partout en France, basés les données officielles." />

    <!-- CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css"/>

    <!-- GOOGLE -->
    <?php include 'assets/google.php';?>

</head>

<body>

    <!-- NAVBAR -->
    <div id="navbar">
        <a href="index.php" id="klima_logo">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 97 83" fill="none">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M94.0632 65.4958C98.3132 55.0158 95.5534 45.6157 88.3434 37.2859C87.3547 34.2632 84.8859 32.9531 82.4333 31.6516C81.8351 31.334 81.238 31.0171 80.6633 30.6758C76.8132 28.9758 72.7531 28.2558 68.5734 28.4458C65.3635 28.5957 62.9333 27.4958 60.7834 25.0158C57.1833 20.8657 52.9933 17.3757 47.6935 15.4858C47.3376 15.3459 46.9826 15.2014 46.6276 15.0571C43.8615 13.9311 41.1017 12.8078 37.9733 13.9158C37.3068 13.9331 36.6374 13.9536 35.968 13.97C35.4724 13.9819 34.9767 13.9917 34.4831 13.9958C15.3835 14.0857 0.253113 28.8056 0.00311252 47.5359C-0.246887 66.2358 14.5934 81.5657 33.6135 81.9158C45.7531 82.1457 57.9133 82.1457 70.0534 81.8957C81.4933 81.6658 89.7634 76.0857 94.0632 65.4958ZM45.9665 73.9748C47.8391 73.9602 49.7116 73.9455 51.5832 73.9455V73.9055C53.5969 73.9055 55.6125 73.9314 57.6286 73.9573C62.1096 74.0151 66.591 74.073 71.0529 73.8454C80.3732 73.3755 87.633 65.7356 87.923 56.6255C88.2228 47.4455 81.403 39.3257 72.383 38.1255C66.9533 37.4055 66.7629 37.6155 67.5031 42.9255C67.7531 44.7256 67.9333 46.5857 67.7829 48.3855C67.613 50.4455 66.1833 51.7256 64.1432 51.8357C62.0832 51.9455 60.673 50.7356 60.1633 48.7456C59.9821 48.0461 59.9377 47.3157 59.8932 46.5857C59.8664 46.1465 59.8395 45.7075 59.7829 45.2756C57.923 31.0056 46.1432 21.1155 32.1232 22.0456C18.4831 22.9555 7.73309 34.7256 7.99286 48.4656C8.25311 62.4155 19.3029 73.4756 33.613 73.9155C37.7243 74.0393 41.8454 74.0071 45.9665 73.9748Z" fill="#E3E3E3"/>
                <path d="M88.3428 37.2856C85.9628 35.6056 83.5729 33.9256 81.1929 32.2556C80.4429 30.5556 80.4729 28.7256 80.3129 26.9156C79.6329 19.2756 75.6329 14.0056 68.6429 11.0656C61.6829 8.14561 55.4129 9.79561 49.6829 14.3356C48.6929 15.1156 48.0129 16.4556 46.4429 16.2856C43.6129 15.4956 40.7929 14.7056 37.9629 13.9156C45.4829 1.43562 60.2228 -3.35439 73.2928 2.44561C86.6628 8.37561 92.6528 22.2356 88.3428 37.2856Z" fill="#316BFF"/>
            </svg>
            <span>Klima</span>
        </a>
        <a href="#!" id="navbar-back_home">
            <i class="fa fa-chevron-left"></i>
            Retour
        </a>
        <div id="navbar-icon">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <!-- NAVBAR SIDEBAR -->
    <div id="navbar_sidebar">
        <a href="index.php" id="klima_logo">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 97 83" fill="none">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M94.0632 65.4958C98.3132 55.0158 95.5534 45.6157 88.3434 37.2859C87.3547 34.2632 84.8859 32.9531 82.4333 31.6516C81.8351 31.334 81.238 31.0171 80.6633 30.6758C76.8132 28.9758 72.7531 28.2558 68.5734 28.4458C65.3635 28.5957 62.9333 27.4958 60.7834 25.0158C57.1833 20.8657 52.9933 17.3757 47.6935 15.4858C47.3376 15.3459 46.9826 15.2014 46.6276 15.0571C43.8615 13.9311 41.1017 12.8078 37.9733 13.9158C37.3068 13.9331 36.6374 13.9536 35.968 13.97C35.4724 13.9819 34.9767 13.9917 34.4831 13.9958C15.3835 14.0857 0.253113 28.8056 0.00311252 47.5359C-0.246887 66.2358 14.5934 81.5657 33.6135 81.9158C45.7531 82.1457 57.9133 82.1457 70.0534 81.8957C81.4933 81.6658 89.7634 76.0857 94.0632 65.4958ZM45.9665 73.9748C47.8391 73.9602 49.7116 73.9455 51.5832 73.9455V73.9055C53.5969 73.9055 55.6125 73.9314 57.6286 73.9573C62.1096 74.0151 66.591 74.073 71.0529 73.8454C80.3732 73.3755 87.633 65.7356 87.923 56.6255C88.2228 47.4455 81.403 39.3257 72.383 38.1255C66.9533 37.4055 66.7629 37.6155 67.5031 42.9255C67.7531 44.7256 67.9333 46.5857 67.7829 48.3855C67.613 50.4455 66.1833 51.7256 64.1432 51.8357C62.0832 51.9455 60.673 50.7356 60.1633 48.7456C59.9821 48.0461 59.9377 47.3157 59.8932 46.5857C59.8664 46.1465 59.8395 45.7075 59.7829 45.2756C57.923 31.0056 46.1432 21.1155 32.1232 22.0456C18.4831 22.9555 7.73309 34.7256 7.99286 48.4656C8.25311 62.4155 19.3029 73.4756 33.613 73.9155C37.7243 74.0393 41.8454 74.0071 45.9665 73.9748Z" fill="#E3E3E3"/>
                <path d="M88.3428 37.2856C85.9628 35.6056 83.5729 33.9256 81.1929 32.2556C80.4429 30.5556 80.4729 28.7256 80.3129 26.9156C79.6329 19.2756 75.6329 14.0056 68.6429 11.0656C61.6829 8.14561 55.4129 9.79561 49.6829 14.3356C48.6929 15.1156 48.0129 16.4556 46.4429 16.2856C43.6129 15.4956 40.7929 14.7056 37.9629 13.9156C45.4829 1.43562 60.2228 -3.35439 73.2928 2.44561C86.6628 8.37561 92.6528 22.2356 88.3428 37.2856Z" fill="#316BFF"/>
            </svg>
            <span>Klima</span>
        </a>
        <!-- 
        <div class="navbar_sidebar-title">Villes populaires</div>
        <div>
            <a class="navbar_sidebar-item" href="#">Lacanau</a>
            <a class="navbar_sidebar-item" href="#">Honfleur, Calvados</a>
            <a class="navbar_sidebar-item" href="#">Saint-Malo, Ille-et-Vilaine</a>
            <a class="navbar_sidebar-item" href="#">Saint-Tropez, Var</a>
            <a class="navbar_sidebar-item" href="#">Étretat, Seine-Maritime</a>
        </div>
        
        <div class="navbar_sidebar-title">Klima</div>
        <div>
            <a class="navbar_sidebar-others" href="#">Contact</a>
            <a class="navbar_sidebar-others" href="#">Mentions légales</a>
        </div>
        -->
        <div class="navbar_sidebar-title">mots-clés</div>
        <div>
            <p class="navbar_sidebar-keywords">Météo</p>
            <p class="navbar_sidebar-keywords">Marée</p>
            <p class="navbar_sidebar-keywords">Marées</p>
            <p class="navbar_sidebar-keywords">Informations</p>
            <p class="navbar_sidebar-keywords">Calendrier</p>
            <p class="navbar_sidebar-keywords">Horaires</p>
            <p class="navbar_sidebar-keywords">Horaire</p>
            <p class="navbar_sidebar-keywords">Coefficient</p>
            <p class="navbar_sidebar-keywords">Direct</p>
        </div>
        <p id="navbar_sidebar-ml">© Klima 2020</p>
    </div>

    <div id="fake_navbar"></div>

    <!-- BACKGROUND POP UP -->
    <div id="background_pop_up"></div>

    <div id="header_home">
        <h1>Météo, marées, heures du soleil ...<span>Disponible pour toutes les villes de France</span></h1>
    </div>

    <div id="search_home" class="search_open">
        <i class="fa fa-search" aria-hidden="true"></i>
        <p>Rechercher par ville</p>
    </div>

    <!-- SEARCH -->
    <div id="search_container">
        <div id="search">
            <input type="text" name="" placeholder="Recherchez par ville, code postal ..." id="search" value="" autocomplete="off">
            <i class="fa fa-chevron-left"></i>
            <i class="fa fa-times-circle"></i>
        </div>
        <div id="search_container-results"></div>
    </div>

    <!-- 
    EN DIRECT 
    <div class="card_container card_container-live">
        <h5>En direct</h5>
        <div class="card_container-scroll">
            <a href="cities/paris.php" class="card_city">
                <img src="img/cities/paris.jpg" alt=""/>
                <h3>Paris</h3>
                <h4><i class="fa fa-map-marker"></i>Ile-de-France</h4>
                <div class="card_city-background"></div>
            </a>
        </div>
    </div>

    GRANDES VILLES
    <div class="card_container card_container-after">
        <h5>Grandes villes</h5>
        <div class="card_container-scroll">
            <a href="#!" class="card_city">
                <img src="img/cities/paris.jpg" alt=""/>
                <h3>Paris</h3>
                <h4><i class="fa fa-map-marker"></i>Ile-de-France</h4>
                <div class="card_city-background"></div>
            </a>
            <a href="#!" class="card_city">
                <img src="img/cities/marseille.jpg" alt=""/>
                <h3>Marseille</h3>
                <h4><i class="fa fa-map-marker"></i>Bouches-du-Rhône</h4>
                <div class="card_city-background"></div>
            </a>
            <a href="#!" class="card_city">
                <img src="img/cities/lyon.jpg" alt=""/>
                <h3>Lyon</h3>
                <h4><i class="fa fa-map-marker"></i>Rhône</h4>
                <div class="card_city-background"></div>
            </a>
            <a href="#!" class="card_city">
                <img src="img/cities/toulouse.jpg" alt=""/>
                <h3>Toulouse</h3>
                <h4><i class="fa fa-map-marker"></i>haute-Garonne</h4>
                <div class="card_city-background"></div>
            </a>
            <a href="#!" class="card_city">
                <img src="img/cities/paris.jpg" alt=""/>
                <h3>Nice</h3>
                <h4><i class="fa fa-map-marker"></i>Alpes-Maritimes</h4>
                <div class="card_city-background"></div>
            </a>
        </div>
    </div>

    PLAGES POPULAIRES
    <div class="card_container card_light_container">
        <h5>Plages populaires</h5>
        <a href="#!" class="card_city">
            <div class="card_light_container-img">
                <img src="https://storage.lebonguide.com/crop-1600x700/97/52/69F8C3ED-4C97-4D13-BC25-31D90B47B181.png" alt=""/>
            </div>
            <div class="card_light_container-text">
                <h3>Lacanau</h3>
                <h4><i class="fa fa-map-marker"></i> Ile-de-France</h4>
            </div>
        </a>
        <a href="#!" class="card_city">
            <div class="card_light_container-img">
                <img src="https://storage.lebonguide.com/crop-1600x700/97/52/69F8C3ED-4C97-4D13-BC25-31D90B47B181.png" alt=""/>
            </div>
            <div class="card_light_container-text">
                <h3>Lacanau</h3>
                <h4><i class="fa fa-map-marker"></i> Ile-de-France</h4>
            </div>
        </a>
        <a href="#!" class="card_city">
            <div class="card_light_container-img">
                <img src="https://storage.lebonguide.com/crop-1600x700/97/52/69F8C3ED-4C97-4D13-BC25-31D90B47B181.png" alt=""/>
            </div>
            <div class="card_light_container-text">
                <h3>Lacanau</h3>
                <h4><i class="fa fa-map-marker"></i> Ile-de-France</h4>
            </div>
        </a>
        <a href="#!" class="card_city">
            <div class="card_light_container-img">
                <img src="https://storage.lebonguide.com/crop-1600x700/97/52/69F8C3ED-4C97-4D13-BC25-31D90B47B181.png" alt=""/>
            </div>
            <div class="card_light_container-text">
                <h3>Lacanau</h3>
                <h4><i class="fa fa-map-marker"></i> Ile-de-France</h4>
            </div>
        </a>
    </div>

    RECENT SEARCH
    <div class="card_container card_container-after">
        <h5>Recherches rècentes</h5>
        <div class="card_container-scroll">
            <a href="#!" class="card_city">
                <img src="img/cities/paris.jpg" alt=""/>
                <h3>Paris</h3>
                <h4><i class="fa fa-map-marker"></i>Ile-de-France</h4>
                <div class="card_city-background"></div>
            </a>
            <a href="#!" class="card_city">
                <img src="img/cities/marseille.jpg" alt=""/>
                <h3>Marseille</h3>
                <h4><i class="fa fa-map-marker"></i>Bouches-du-Rhône</h4>
                <div class="card_city-background"></div>
            </a>
            <a href="#!" class="card_city">
                <img src="img/cities/lyon.jpg" alt=""/>
                <h3>Lyon</h3>
                <h4><i class="fa fa-map-marker"></i>Rhône</h4>
                <div class="card_city-background"></div>
            </a>
            <a href="#!" class="card_city">
                <img src="img/cities/toulouse.jpg" alt=""/>
                <h3>Toulouse</h3>
                <h4><i class="fa fa-map-marker"></i>haute-Garonne</h4>
                <div class="card_city-background"></div>
            </a>
            <a href="#!" class="card_city">
                <img src="img/cities/paris.jpg" alt=""/>
                <h3>Nice</h3>
                <h4><i class="fa fa-map-marker"></i>Alpes-Maritimes</h4>
                <div class="card_city-background"></div>
            </a>
        </div>
    </div>
    -->

    <!-- PAGE -->
    <div id="page_container">
        <div class="page_container-header">
        </div>
        <div class="page_container-contain">
            <div class="page_container-contain-item data-temperature">
            </div>
            <div class="page_container-contain-item data-sunrise">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86 80" fill="none">
                    <path d="M63.1712 42.4506C62.3045 30.522 53.9172 22.2372 42.7061 22.2372C31.4951 22.2372 23.1078 30.5127 22.2411 42.4506C19.7529 44.8922 17.2646 44.8922 14.7671 42.4506C15.5499 26.4961 27.3853 14.7725 42.7061 14.7725C58.0363 14.7725 69.8718 26.4961 70.6453 42.4506C68.1477 44.8922 65.6595 44.8922 63.1712 42.4506Z" fill="#FDC930"/>
                    <path d="M46.4709 5.54636C46.5455 8.21166 46.0515 10.8304 42.8457 10.8957C39.3044 10.9702 38.941 8.18375 38.9224 5.36003C38.9037 2.67609 39.3977 0.0667028 42.5755 0.00146815C46.0703 -0.0730857 46.5548 2.704 46.4709 5.54636Z" fill="#F1C12F"/>
                    <path d="M74.1212 15.1637C73.9628 17.4749 69.6945 21.4449 67.635 21.2212C65.2679 20.9603 64.0192 19.5252 64.2242 17.1301C64.4292 14.7444 68.716 11.026 70.8035 11.4547C72.8258 11.8554 73.9535 13.244 74.1212 15.1637Z" fill="#957C2C"/>
                    <path d="M17.8238 21.2492C15.7643 21.3797 11.347 17.5401 11.3377 15.3967C11.3284 13.0576 12.7169 11.6224 15.0187 11.4267C17.1901 11.2496 21.1974 15.3408 21.244 17.5401C21.2813 19.4412 19.725 21.1281 17.8238 21.2492Z" fill="#957C2C"/>
                    <path d="M14.7671 42.4504C17.2553 42.8605 19.7436 42.8512 22.2411 42.4504C27.9725 42.5343 33.7131 42.6462 39.4444 42.6927C42.0351 42.7114 44.3184 43.373 44.393 46.3365C44.4675 49.3187 42.2216 50.1667 39.6215 50.1667C27.9818 50.1761 16.3514 50.1761 4.71171 50.1667C2.10233 50.1667 -0.125012 49.2628 0.00545722 46.2806C0.126607 43.3171 2.4471 42.7114 5.01921 42.6927C8.2623 42.6648 11.5147 42.5343 14.7671 42.4504Z" fill="#0082FB"/>
                    <path d="M54.355 65.087C49.0803 65.087 43.8056 65.0777 38.5309 65.0963C35.9495 65.1057 33.6477 64.4347 33.5638 61.4618C33.48 58.489 35.6886 57.6223 38.3166 57.6223C49.332 57.6223 60.3472 57.613 71.3626 57.6223C73.9813 57.6223 76.19 58.4983 76.0968 61.4712C76.0036 64.444 73.6832 65.0963 71.1111 65.087C65.5195 65.0777 59.9372 65.087 54.355 65.087Z" fill="#0082FA"/>
                    <path d="M63.1712 42.4504C65.6594 42.8512 68.1476 42.8605 70.6452 42.4504C74.0467 42.5343 77.4575 42.6461 80.859 42.702C83.31 42.7486 85.3416 43.606 85.4162 46.2713C85.5093 49.2535 83.3193 50.1574 80.7099 50.1667C72.1828 50.1854 63.6464 50.1761 55.1193 50.1761C52.5006 50.1761 50.2733 49.3094 50.3386 46.3552C50.4038 43.401 52.6777 42.7021 55.2778 42.6927C57.9151 42.6834 60.5431 42.5343 63.1712 42.4504Z" fill="#0082FA"/>
                    <path d="M32.4924 79.9885C29.3891 79.9885 26.2858 79.9885 23.1918 79.9885C20.7409 79.9885 18.7465 79.1031 18.6254 76.4285C18.5042 73.7352 20.3961 72.5797 22.8657 72.5517C29.2214 72.4865 35.5864 72.4958 41.9421 72.5424C44.4024 72.561 46.3315 73.6421 46.2756 76.3353C46.2197 79.01 44.2719 79.9698 41.8116 79.9885C38.699 79.9978 35.5957 79.9885 32.4924 79.9885Z" fill="#0081F9"/>
                    <path d="M18.5598 57.6597C20.4237 57.6597 22.2968 57.5385 24.1421 57.6876C26.2668 57.8647 27.6368 59.1694 27.6461 61.3222C27.6554 63.4749 26.2948 64.8821 24.1794 64.9753C20.461 65.143 16.7239 65.1524 13.0055 65.0033C10.713 64.9101 9.15677 63.5588 9.2686 61.1264C9.37111 58.9551 10.8808 57.8181 12.9683 57.669C14.8228 57.5386 16.696 57.641 18.5505 57.641C18.5598 57.6504 18.5598 57.6597 18.5598 57.6597Z" fill="#6AB3F7"/>
                    <path d="M59.4435 79.9977C58.5209 79.9977 57.5982 79.9977 56.6849 79.9977C54.2339 80.007 52.2769 79.0471 52.1838 76.4471C52.0719 73.5488 54.1502 72.5423 56.7689 72.5051C58.6141 72.4771 60.4499 72.4678 62.2951 72.5051C64.6901 72.5517 66.7125 73.4277 66.8057 76.0743C66.8989 78.9913 64.8486 79.9697 62.2206 79.9884C61.2887 80.007 60.3661 79.9977 59.4435 79.9977Z" fill="#69B3F6"/>
                </svg>
            </div>
            <div class="page_container-contain-item data-sunset">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86 80" fill="none">
                    <path d="M7.22944 44.4887C5.13322 23.0073 17.5663 5.91056 37.74 2.41045C40.5478 1.92005 42.1825 1.64121 41.7017 5.61249C39.0574 27.7574 58.8561 42.0078 76.0585 37.9789C79.3663 37.2 80.5202 38.3154 79.7894 41.604C79.4432 43.1617 79.2317 44.7483 78.9625 46.3253C74.1162 48.2484 69.2794 45.7772 64.3947 46.3061C61.8273 46.5849 58.8945 46.9696 56.8271 44.5079C56.3175 43.3829 55.2213 43.0559 54.2309 42.5463C43.144 36.825 36.2977 27.8536 34.2976 15.5263C33.6918 11.805 32.6341 11.5261 29.5475 13.2088C18.5663 19.2186 12.8066 31.546 15.1816 44.4694C12.5373 46.9022 9.89298 46.883 7.22944 44.4887Z" fill="#FDCA30"/>
                    <circle cx="54.5" cy="8.5" r="4.5" fill="#C39D2D"/>
                    <circle cx="68.5" cy="23.5" r="4.5" fill="#F2C12F"/>
                    <circle cx="73.5" cy="4.5" r="4.5" fill="#977D2C"/>
                    <path d="M14.7671 42.4507C17.2553 42.8607 19.7436 42.8514 22.2411 42.4507C27.9725 42.5346 33.7131 42.6464 39.4444 42.693C42.0351 42.7116 44.3184 43.3733 44.393 46.3368C44.4675 49.3189 42.2216 50.167 39.6215 50.167C27.9818 50.1763 16.3514 50.1763 4.71171 50.167C2.10233 50.167 -0.125012 49.263 0.00545722 46.2809C0.126607 43.3174 2.4471 42.7116 5.01921 42.693C8.2623 42.665 11.5147 42.5346 14.7671 42.4507Z" fill="#0082FB"/>
                    <path d="M54.355 65.0873C49.0803 65.0873 43.8056 65.078 38.5309 65.0966C35.9495 65.1059 33.6477 64.4349 33.5638 61.4621C33.48 58.4892 35.6886 57.6226 38.3166 57.6226C49.332 57.6226 60.3472 57.6132 71.3626 57.6226C73.9813 57.6226 76.19 58.4986 76.0968 61.4714C76.0036 64.4442 73.6832 65.0966 71.1111 65.0873C65.5195 65.0779 59.9372 65.0873 54.355 65.0873Z" fill="#0082FA"/>
                    <path d="M63.1712 42.4507C65.6594 42.8514 68.1476 42.8607 70.6452 42.4507C74.0467 42.5346 77.4575 42.6463 80.859 42.7023C83.31 42.7489 85.3416 43.6062 85.4162 46.2715C85.5093 49.2537 83.3193 50.1577 80.7099 50.167C72.1828 50.1856 63.6464 50.1763 55.1193 50.1763C52.5006 50.1763 50.2733 49.3096 50.3386 46.3554C50.4038 43.4012 52.6777 42.7023 55.2778 42.693C57.9151 42.6837 60.5431 42.5346 63.1712 42.4507Z" fill="#0082FA"/>
                    <path d="M32.4924 79.9887C29.3891 79.9887 26.2858 79.9887 23.1918 79.9887C20.7409 79.9887 18.7465 79.1034 18.6254 76.4287C18.5042 73.7355 20.3961 72.5799 22.8657 72.552C29.2214 72.4867 35.5864 72.496 41.9421 72.5426C44.4024 72.5613 46.3315 73.6423 46.2756 76.3356C46.2197 79.0102 44.2719 79.9701 41.8116 79.9887C38.699 79.998 35.5957 79.9887 32.4924 79.9887Z" fill="#0081F9"/>
                    <path d="M18.5598 57.6599C20.4237 57.6599 22.2968 57.5388 24.1421 57.6879C26.2668 57.8649 27.6368 59.1697 27.6461 61.3224C27.6554 63.4751 26.2948 64.8823 24.1794 64.9755C20.461 65.1433 16.7239 65.1526 13.0055 65.0035C10.713 64.9103 9.15677 63.559 9.2686 61.1267C9.37111 58.9553 10.8808 57.8184 12.9683 57.6693C14.8228 57.5388 16.696 57.6413 18.5505 57.6413C18.5598 57.6506 18.5598 57.6599 18.5598 57.6599Z" fill="#6AB3F7"/>
                    <path d="M59.4435 79.9979C58.5209 79.9979 57.5982 79.9979 56.6849 79.9979C54.2339 80.0073 52.2769 79.0474 52.1838 76.4473C52.0719 73.549 54.1502 72.5426 56.7689 72.5053C58.6141 72.4773 60.4499 72.468 62.2951 72.5053C64.6901 72.5519 66.7125 73.4279 66.8057 76.0746C66.8989 78.9915 64.8486 79.97 62.2206 79.9886C61.2887 80.0072 60.3661 79.9979 59.4435 79.9979Z" fill="#69B3F6"/>
                </svg>
            </div>
        </div>
        <div class="page_container-details">
            <h2>Infos marées</h2>
        </div>
    </div>
    
    <?php include 'assets/footer.php';?>

    <!-- LOADING -->
    <div id="loading_container">
        <svg xmlns="http://www.w3.org/2000/svg" width="97" height="81" viewBox="0 0 97 81" fill="none">
            <path d="M81.1929 32.2556C83.5729 33.9256 85.9628 35.6056 88.3428 37.2856C92.6528 22.2356 86.6628 8.37561 73.2928 2.44561C60.2228 -3.35439 45.4829 1.43562 37.9629 13.9156C39.3781 14.3107 40.7909 14.7057 42.2036 15.1008C43.613 15.495 45.0225 15.8891 46.4344 16.2832C51.055 8.76508 60.7738 5.95609 69.6129 9.5857C78.7029 13.3257 83.4329 22.5757 81.1929 32.2457L81.188 32.2446C81.1896 32.2483 81.1912 32.2519 81.1929 32.2556Z" fill="#316BFF"/>
            <path d="M88.3431 35.2858C95.5531 43.6158 98.3131 53.0158 94.0631 63.4958C89.7631 74.0858 81.4931 79.6658 70.0531 79.8958C57.9131 80.1458 45.7531 80.1458 33.6131 79.9158C14.5931 79.5658 -0.246887 64.2358 0.00311259 45.5358C0.253113 26.8058 15.3831 12.0858 34.4831 11.9958C35.6431 11.9858 36.8131 11.9458 37.9731 11.9158C41.5031 10.6658 44.5631 12.2558 47.6931 13.4858C52.9931 15.3758 57.1831 18.8658 60.7831 23.0158C62.9331 25.4958 65.3631 26.5958 68.5731 26.4458C72.7531 26.2558 76.8131 26.9758 80.6631 28.6758C83.5931 30.4158 87.1131 31.5258 88.3431 35.2858Z" fill="#E3E3E3"/>
            <path d="M51.5829 71.9456C45.5929 71.9456 39.5929 72.0956 33.6129 71.9156C19.3029 71.4756 8.2529 60.4156 7.9929 46.4656C7.7329 32.7256 18.4829 20.9556 32.1229 20.0456C46.1429 19.1156 57.9229 29.0056 59.7829 43.2756C59.9329 44.4256 59.8729 45.6256 60.1629 46.7456C60.6729 48.7356 62.0829 49.9456 64.1429 49.8356C66.1829 49.7256 67.6129 48.4456 67.7829 46.3856C67.9329 44.5856 67.7529 42.7256 67.5029 40.9256C66.7629 35.6156 66.9529 35.4056 72.3829 36.1256C81.4029 37.3256 88.2229 45.4456 87.9229 54.6256C87.6329 63.7356 80.3729 71.3756 71.0529 71.8456C64.5829 72.1756 58.0729 71.9056 51.5829 71.9056C51.5829 71.9156 51.5829 71.9356 51.5829 71.9456Z" fill="white"/>
        </svg>
    </div>
                    
    <!-- SCRIPTS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="http://gd.geobytes.com/gd?after=-1&variables=GeobytesCountry,GeobytesCity"></script>
    <script>
        /**CALL WEATHER UNIQUE 
        var weatherSettings = {
            "async": true,
            "crossDomain": true,
            "url": 'https://api.openweathermap.org/data/2.5/weather?q=Paris&appid=d9b111b0a4dfbc39333a7c680a49e754&units=metric&lang=fr',
            "method": "GET"
        }
        $.ajax(weatherSettings).done(function(response) {
            console.log(response);
            let temp = response.main.temp.toString();
            let humidity = response.main.humidity;
            let description = response.weather[0].description;
            $('<p>' + Math.round(temp) + '°C</p>').appendTo('.card_container-live .card_container-scroll a');
            $('<h5>' + description + '</h5>').appendTo('.card_container-live .card_container-scroll a');
            $('<span>Humidité : ' + humidity + '%</span>').appendTo('.card_container-live .card_container-scroll a h5');
        });
        **/
    </script>
    <script src="js/scripts.js"></script>
</body>

</html>