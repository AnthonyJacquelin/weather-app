
<!DOCTYPE html>
<html lang="FR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Klima - Lyon : Meteo, maree et infos</title>
    <meta name="description" content="L'essentiel des informations sur les marées, hauteur et coefficient. Prédictions et calendrier météo partout en France, basés les données officielles.">
    <meta name="keywords" content="klima clima climat maree ports annuaire calendrier horaire marée marre heure hauteur coefficient météo meteo temps temperature température france lyon">
    <meta name="Content-Language" content="fr">
    <meta name="Subject" content="Marées et météo partout en France">
    <meta name="Copyright" content="Klima">
    <meta name="Author" content="Klima">
    <link rel="shortcut icon" type="image/x-icon" href="../img/logotype.png">

    <!-- TWITTER -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@Meteo-maree.info">
    <meta name="twitter:creator" content="@Meteo-maree.info">

    <!-- FACEBOOK -->
    <meta property="og:title" content="Klima : Meteo, maree et infos">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://meteo-maree.info/">
    <meta property="og:image" content="https://meteo-maree.info/img/social_network.png">
    <meta property="og:description" content="L'essentiel des informations sur les marées, hauteur et coefficient. Prédictions et calendrier météo partout en France, basés les données officielles." />

    <!-- CSS -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/styles.css"/>

    <!-- GOOGLE -->
    <?php include '../assets/google.php';?>

</head>

<body class="city_page">


    <?php include '../assets/nav.php';?>

    <div id="page_container-header-background"></div>

    <div id="page_container-video">
        <img src="lyon.png" alt="lyon ville france">
    </div>

    <div class="page_container-header"></div>
    <div class="page_container-contain">
        <div class="page_container-contain-item data-temperature"></div>
        <div class="page_container-contain-item data-sunrise">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86 80" fill="none">
                <path d="M63.1712 42.4506C62.3045 30.522 53.9172 22.2372 42.7061 22.2372C31.4951 22.2372 23.1078 30.5127 22.2411 42.4506C19.7529 44.8922 17.2646 44.8922 14.7671 42.4506C15.5499 26.4961 27.3853 14.7725 42.7061 14.7725C58.0363 14.7725 69.8718 26.4961 70.6453 42.4506C68.1477 44.8922 65.6595 44.8922 63.1712 42.4506Z" fill="#FDC930"/>
                <path d="M46.4709 5.54636C46.5455 8.21166 46.0515 10.8304 42.8457 10.8957C39.3044 10.9702 38.941 8.18375 38.9224 5.36003C38.9037 2.67609 39.3977 0.0667028 42.5755 0.00146815C46.0703 -0.0730857 46.5548 2.704 46.4709 5.54636Z" fill="#F1C12F"/>
                <path d="M74.1212 15.1637C73.9628 17.4749 69.6945 21.4449 67.635 21.2212C65.2679 20.9603 64.0192 19.5252 64.2242 17.1301C64.4292 14.7444 68.716 11.026 70.8035 11.4547C72.8258 11.8554 73.9535 13.244 74.1212 15.1637Z" fill="#957C2C"/>
                <path d="M17.8238 21.2492C15.7643 21.3797 11.347 17.5401 11.3377 15.3967C11.3284 13.0576 12.7169 11.6224 15.0187 11.4267C17.1901 11.2496 21.1974 15.3408 21.244 17.5401C21.2813 19.4412 19.725 21.1281 17.8238 21.2492Z" fill="#957C2C"/>
                <path d="M14.7671 42.4504C17.2553 42.8605 19.7436 42.8512 22.2411 42.4504C27.9725 42.5343 33.7131 42.6462 39.4444 42.6927C42.0351 42.7114 44.3184 43.373 44.393 46.3365C44.4675 49.3187 42.2216 50.1667 39.6215 50.1667C27.9818 50.1761 16.3514 50.1761 4.71171 50.1667C2.10233 50.1667 -0.125012 49.2628 0.00545722 46.2806C0.126607 43.3171 2.4471 42.7114 5.01921 42.6927C8.2623 42.6648 11.5147 42.5343 14.7671 42.4504Z" fill="#0082FB"/>
                <path d="M54.355 65.087C49.0803 65.087 43.8056 65.0777 38.5309 65.0963C35.9495 65.1057 33.6477 64.4347 33.5638 61.4618C33.48 58.489 35.6886 57.6223 38.3166 57.6223C49.332 57.6223 60.3472 57.613 71.3626 57.6223C73.9813 57.6223 76.19 58.4983 76.0968 61.4712C76.0036 64.444 73.6832 65.0963 71.1111 65.087C65.5195 65.0777 59.9372 65.087 54.355 65.087Z" fill="#0082FA"/>
                <path d="M63.1712 42.4504C65.6594 42.8512 68.1476 42.8605 70.6452 42.4504C74.0467 42.5343 77.4575 42.6461 80.859 42.702C83.31 42.7486 85.3416 43.606 85.4162 46.2713C85.5093 49.2535 83.3193 50.1574 80.7099 50.1667C72.1828 50.1854 63.6464 50.1761 55.1193 50.1761C52.5006 50.1761 50.2733 49.3094 50.3386 46.3552C50.4038 43.401 52.6777 42.7021 55.2778 42.6927C57.9151 42.6834 60.5431 42.5343 63.1712 42.4504Z" fill="#0082FA"/>
                <path d="M32.4924 79.9885C29.3891 79.9885 26.2858 79.9885 23.1918 79.9885C20.7409 79.9885 18.7465 79.1031 18.6254 76.4285C18.5042 73.7352 20.3961 72.5797 22.8657 72.5517C29.2214 72.4865 35.5864 72.4958 41.9421 72.5424C44.4024 72.561 46.3315 73.6421 46.2756 76.3353C46.2197 79.01 44.2719 79.9698 41.8116 79.9885C38.699 79.9978 35.5957 79.9885 32.4924 79.9885Z" fill="#0081F9"/>
                <path d="M18.5598 57.6597C20.4237 57.6597 22.2968 57.5385 24.1421 57.6876C26.2668 57.8647 27.6368 59.1694 27.6461 61.3222C27.6554 63.4749 26.2948 64.8821 24.1794 64.9753C20.461 65.143 16.7239 65.1524 13.0055 65.0033C10.713 64.9101 9.15677 63.5588 9.2686 61.1264C9.37111 58.9551 10.8808 57.8181 12.9683 57.669C14.8228 57.5386 16.696 57.641 18.5505 57.641C18.5598 57.6504 18.5598 57.6597 18.5598 57.6597Z" fill="#6AB3F7"/>
                <path d="M59.4435 79.9977C58.5209 79.9977 57.5982 79.9977 56.6849 79.9977C54.2339 80.007 52.2769 79.0471 52.1838 76.4471C52.0719 73.5488 54.1502 72.5423 56.7689 72.5051C58.6141 72.4771 60.4499 72.4678 62.2951 72.5051C64.6901 72.5517 66.7125 73.4277 66.8057 76.0743C66.8989 78.9913 64.8486 79.9697 62.2206 79.9884C61.2887 80.007 60.3661 79.9977 59.4435 79.9977Z" fill="#69B3F6"/>
            </svg>
        </div>
        <div class="page_container-contain-item data-sunset">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86 80" fill="none">
                <path d="M7.22944 44.4887C5.13322 23.0073 17.5663 5.91056 37.74 2.41045C40.5478 1.92005 42.1825 1.64121 41.7017 5.61249C39.0574 27.7574 58.8561 42.0078 76.0585 37.9789C79.3663 37.2 80.5202 38.3154 79.7894 41.604C79.4432 43.1617 79.2317 44.7483 78.9625 46.3253C74.1162 48.2484 69.2794 45.7772 64.3947 46.3061C61.8273 46.5849 58.8945 46.9696 56.8271 44.5079C56.3175 43.3829 55.2213 43.0559 54.2309 42.5463C43.144 36.825 36.2977 27.8536 34.2976 15.5263C33.6918 11.805 32.6341 11.5261 29.5475 13.2088C18.5663 19.2186 12.8066 31.546 15.1816 44.4694C12.5373 46.9022 9.89298 46.883 7.22944 44.4887Z" fill="#FDCA30"/>
                <circle cx="54.5" cy="8.5" r="4.5" fill="#C39D2D"/>
                <circle cx="68.5" cy="23.5" r="4.5" fill="#F2C12F"/>
                <circle cx="73.5" cy="4.5" r="4.5" fill="#977D2C"/>
                <path d="M14.7671 42.4507C17.2553 42.8607 19.7436 42.8514 22.2411 42.4507C27.9725 42.5346 33.7131 42.6464 39.4444 42.693C42.0351 42.7116 44.3184 43.3733 44.393 46.3368C44.4675 49.3189 42.2216 50.167 39.6215 50.167C27.9818 50.1763 16.3514 50.1763 4.71171 50.167C2.10233 50.167 -0.125012 49.263 0.00545722 46.2809C0.126607 43.3174 2.4471 42.7116 5.01921 42.693C8.2623 42.665 11.5147 42.5346 14.7671 42.4507Z" fill="#0082FB"/>
                <path d="M54.355 65.0873C49.0803 65.0873 43.8056 65.078 38.5309 65.0966C35.9495 65.1059 33.6477 64.4349 33.5638 61.4621C33.48 58.4892 35.6886 57.6226 38.3166 57.6226C49.332 57.6226 60.3472 57.6132 71.3626 57.6226C73.9813 57.6226 76.19 58.4986 76.0968 61.4714C76.0036 64.4442 73.6832 65.0966 71.1111 65.0873C65.5195 65.0779 59.9372 65.0873 54.355 65.0873Z" fill="#0082FA"/>
                <path d="M63.1712 42.4507C65.6594 42.8514 68.1476 42.8607 70.6452 42.4507C74.0467 42.5346 77.4575 42.6463 80.859 42.7023C83.31 42.7489 85.3416 43.6062 85.4162 46.2715C85.5093 49.2537 83.3193 50.1577 80.7099 50.167C72.1828 50.1856 63.6464 50.1763 55.1193 50.1763C52.5006 50.1763 50.2733 49.3096 50.3386 46.3554C50.4038 43.4012 52.6777 42.7023 55.2778 42.693C57.9151 42.6837 60.5431 42.5346 63.1712 42.4507Z" fill="#0082FA"/>
                <path d="M32.4924 79.9887C29.3891 79.9887 26.2858 79.9887 23.1918 79.9887C20.7409 79.9887 18.7465 79.1034 18.6254 76.4287C18.5042 73.7355 20.3961 72.5799 22.8657 72.552C29.2214 72.4867 35.5864 72.496 41.9421 72.5426C44.4024 72.5613 46.3315 73.6423 46.2756 76.3356C46.2197 79.0102 44.2719 79.9701 41.8116 79.9887C38.699 79.998 35.5957 79.9887 32.4924 79.9887Z" fill="#0081F9"/>
                <path d="M18.5598 57.6599C20.4237 57.6599 22.2968 57.5388 24.1421 57.6879C26.2668 57.8649 27.6368 59.1697 27.6461 61.3224C27.6554 63.4751 26.2948 64.8823 24.1794 64.9755C20.461 65.1433 16.7239 65.1526 13.0055 65.0035C10.713 64.9103 9.15677 63.559 9.2686 61.1267C9.37111 58.9553 10.8808 57.8184 12.9683 57.6693C14.8228 57.5388 16.696 57.6413 18.5505 57.6413C18.5598 57.6506 18.5598 57.6599 18.5598 57.6599Z" fill="#6AB3F7"/>
                <path d="M59.4435 79.9979C58.5209 79.9979 57.5982 79.9979 56.6849 79.9979C54.2339 80.0073 52.2769 79.0474 52.1838 76.4473C52.0719 73.549 54.1502 72.5426 56.7689 72.5053C58.6141 72.4773 60.4499 72.468 62.2951 72.5053C64.6901 72.5519 66.7125 73.4279 66.8057 76.0746C66.8989 78.9915 64.8486 79.97 62.2206 79.9886C61.2887 80.0072 60.3661 79.9979 59.4435 79.9979Z" fill="#69B3F6"/>
            </svg>
        </div>
    </div>

    <div class="page_container-details">
        <h2>Infos marées</h2>
    </div>
    
    <!-- LOADING -->
    <div id="loading_container">
        <svg xmlns="http://www.w3.org/2000/svg" width="97" height="81" viewBox="0 0 97 81" fill="none">
            <path d="M81.1929 32.2556C83.5729 33.9256 85.9628 35.6056 88.3428 37.2856C92.6528 22.2356 86.6628 8.37561 73.2928 2.44561C60.2228 -3.35439 45.4829 1.43562 37.9629 13.9156C39.3781 14.3107 40.7909 14.7057 42.2036 15.1008C43.613 15.495 45.0225 15.8891 46.4344 16.2832C51.055 8.76508 60.7738 5.95609 69.6129 9.5857C78.7029 13.3257 83.4329 22.5757 81.1929 32.2457L81.188 32.2446C81.1896 32.2483 81.1912 32.2519 81.1929 32.2556Z" fill="#316BFF"/>
            <path d="M88.3431 35.2858C95.5531 43.6158 98.3131 53.0158 94.0631 63.4958C89.7631 74.0858 81.4931 79.6658 70.0531 79.8958C57.9131 80.1458 45.7531 80.1458 33.6131 79.9158C14.5931 79.5658 -0.246887 64.2358 0.00311259 45.5358C0.253113 26.8058 15.3831 12.0858 34.4831 11.9958C35.6431 11.9858 36.8131 11.9458 37.9731 11.9158C41.5031 10.6658 44.5631 12.2558 47.6931 13.4858C52.9931 15.3758 57.1831 18.8658 60.7831 23.0158C62.9331 25.4958 65.3631 26.5958 68.5731 26.4458C72.7531 26.2558 76.8131 26.9758 80.6631 28.6758C83.5931 30.4158 87.1131 31.5258 88.3431 35.2858Z" fill="#E3E3E3"/>
            <path d="M51.5829 71.9456C45.5929 71.9456 39.5929 72.0956 33.6129 71.9156C19.3029 71.4756 8.2529 60.4156 7.9929 46.4656C7.7329 32.7256 18.4829 20.9556 32.1229 20.0456C46.1429 19.1156 57.9229 29.0056 59.7829 43.2756C59.9329 44.4256 59.8729 45.6256 60.1629 46.7456C60.6729 48.7356 62.0829 49.9456 64.1429 49.8356C66.1829 49.7256 67.6129 48.4456 67.7829 46.3856C67.9329 44.5856 67.7529 42.7256 67.5029 40.9256C66.7629 35.6156 66.9529 35.4056 72.3829 36.1256C81.4029 37.3256 88.2229 45.4456 87.9229 54.6256C87.6329 63.7356 80.3729 71.3756 71.0529 71.8456C64.5829 72.1756 58.0729 71.9056 51.5829 71.9056C51.5829 71.9156 51.5829 71.9356 51.5829 71.9456Z" fill="white"/>
        </svg>
    </div>
    
    <?php include '../assets/footer.php';?>

    <!-- SCRIPTS -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="http://gd.geobytes.com/gd?after=-1&variables=GeobytesCountry,GeobytesCity"></script>
    <script>
        $(document).ready(function() {

            /** INIT */
            const apiUrl = 'https://api-adresse.data.gouv.fr/search/?q=';
            const format = '&type=municipality';
            const windowWidth = window.screen.width;

            if (windowWidth >= 1200) {
                $('body').addClass('ui_desktop');
            }
            if (windowWidth <= 768) {
                $('body').addClass('ui_mobile');
            }

            let value = 'lyon';
            let url = apiUrl + value + format;
        
            $.getJSON(url, function(json) {
                //console.log(json);
                let city = json.features[0].properties.city;
                var postalCodeContext1 = json.features[0].properties.context.toString().split(',').slice(1);
                var postalCodeContext2 = json.features[0].properties.context.toString().split(',').slice(2);
                let postalCode = json.features[0].properties.postcode;
                let postalCodeVideo = postalCode.toString().substring(0, 2);
                let department = json.features[0].properties.context.toString().split(',').slice(1);
                let population = json.features[0].properties.population;
                let coordinates = json.features[0].geometry.coordinates;
                coordinates.toString().split(",");
                let coordinatesLatitude = coordinates[1];
                let coordinatesLongitude = coordinates[0];
                $('<div class="search_container-results-item" style="opacity: 0; position: absolute; top: 0; left: -100%; visibility: hidden;" data-population=' + population + ' data-postalCodeContext1=' + postalCodeContext1 + ' data-postalCodeContext2=' + postalCodeContext2 + ' data-postalCode=' + postalCode + ' data-postalCodeVideo=' + postalCodeVideo + ' data-coordinatesLatitude=' + coordinatesLatitude + ' data-coordinatesLongitude=' + coordinatesLongitude + '><span>' + city + '</span><b>' + postalCode + department + '</b></div>').appendTo('body');
            }).done(function() {

                    var SearchItem = $('.search_container-results-item');
                    let cityName = 'lyon';
                    let url = '/cities/' + cityName + '.php';

                    //INIT
                    let coordinatesLatitude = SearchItem.attr('data-coordinatesLatitude');
                    let coordinatesLongitude = SearchItem.attr('data-coordinatesLongitude');
                    cityName.toString().split(",");
                    let population = SearchItem.attr('data-population');
                    let postalCode = SearchItem.attr('data-postalCode');
                    let postalCodeContext1 = SearchItem.attr('data-postalCodeContext1');
                    let postalCodeContext2 = SearchItem.attr('data-postalCodeContext2');

                    $('<h1>' + cityName + '</h1>').appendTo('.page_container-header');
                    $('<h2>' + postalCode + ', ' + postalCodeContext1 + ' ' + postalCodeContext2 + '</h2>').appendTo('.page_container-header');
                    $('<h5>Lat. ' + coordinatesLatitude + ' Long. ' + coordinatesLongitude + '</h5>').appendTo('.page_container-header');

                    postalCode = postalCode.slice(0, 2);

                    if (postalCode != "33" && postalCode != "59" && postalCode != "62" && postalCode != "80" && postalCode != "76" && postalCode != "27" && postalCode != "17" && postalCode != "50" && postalCode != "35" && postalCode != "22" && postalCode != "29" && postalCode != "56" && postalCode != "44" && postalCode != "85" && postalCode != "17" && postalCode != "33" && postalCode != "40" && postalCode != "64" && postalCode != "66" && postalCode != "11" && postalCode != "34" && postalCode != "30" && postalCode != "13" && postalCode != "83" && postalCode != "06"){
                        $('.page_container-details').remove();
                    }
                    else{
                        //TIDES
                        let urlTides = "https://tides.p.rapidapi.com/tides?radius=200&latitude=" + coordinatesLatitude + "&longitude=" + coordinatesLongitude;
                        urlTides.toString();
                        var tideSettings = {
                            "async": true,
                            "crossDomain": true,
                            "url": urlTides,
                            "method": "GET",
                            "headers": {
                                "x-rapidapi-host": "tides.p.rapidapi.com",
                                "x-rapidapi-key": "6097a8665bmsh822c1af153036c2p1d8618jsn6ebabe298715"
                            }
                        }
                        $.ajax(tideSettings).done(function(response) {
                            $.each(response.extremes, function(i, obj) {
                                //console.log(obj);
                                let datetimeHours = obj.datetime.toString().split('T').slice(1).toString().split('+').slice(0).toString().split(',')[0].substring(0, 2).toString();
                                let datetimeMinutes = obj.datetime.toString().split('T').slice(1).toString().split('+').slice(0).toString().split(',')[0].substring(3, 5).toString();
                                if (datetimeHours == 23) {
                                    datetimeHours = 0;
                                } else {
                                    datetimeHours++;
                                }
                                if (datetimeHours == 23) {
                                    datetimeHours = 0;
                                } else {
                                    datetimeHours++;
                                }
                                let state = obj.state;
                                if (state == 'HIGH TIDE') {
                                    state = 'Haute';
                                    $('<h4 class="page_container-details-tide page_container-details-tide_high"><b>' + datetimeHours + 'h' + datetimeMinutes + '</b><span>' + state + '</span></h4>').appendTo('.page_container-details');

                                } else {
                                    state = 'Basse';
                                    $('<h4 class="page_container-details-tide page_container-details-tide_low"><b>' + datetimeHours + 'h' + datetimeMinutes + '</b><span>' + state + '</span></h4>').appendTo('.page_container-details');
                                }
                            });
                        });
                    }

                    //WEATHER
                    var weatherSettings = {
                        "async": true,
                        "crossDomain": true,
                        "url": 'https://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&appid=d9b111b0a4dfbc39333a7c680a49e754&units=metric&lang=fr',
                        "method": "GET"
                    }
                    $.ajax(weatherSettings).done(function(response) {
                        //console.log(response);

                        let sunrise = response.sys.sunrise;
                        let sunset = response.sys.sunset;

                        let unix_timestamp = sunrise;
                        var date = new Date(unix_timestamp * 1000);
                        var hours = date.getHours();
                        var minutes = "0" + date.getMinutes();
                        var formattedTime = hours + 'h' + minutes.substr(-2);
                        $('<h3>Lever soleil</h3>').appendTo('.page_container-contain .data-sunrise');
                        $('<h4>' + formattedTime + '</h4>').appendTo('.page_container-contain .data-sunrise');

                        unix_timestamp = sunset;
                        date = new Date(unix_timestamp * 1000);
                        hours = date.getHours();
                        minutes = "0" + date.getMinutes();
                        seconds = "0" + date.getSeconds();
                        formattedTime = hours + 'h' + minutes.substr(-2);
                        $('<h3>Coucher soleil</h3>').appendTo('.page_container-contain .data-sunset');
                        $('<h4>' + formattedTime + '</h4>').appendTo('.page_container-contain .data-sunset');


                        let temperature = response.main.temp;
                        let humidity = response.main.humidity;
                        let description = response.weather[0].description;
                        let wind = response.wind.speed;
                        var convert = (function() {
                            var conversions = {
                                speed: {
                                    ms: 1, // use m/s as our base unit
                                    kmh: 3.6,
                                    mph: 2.23693629,
                                    knots: 1.94384449
                                },

                                distance: {
                                    m: 1, // use meters as our base
                                    inches: 39.3700787402, // can't use "in" as that's a keyword. Darn.
                                    ft: 3.280839895,
                                    mi: 0.000621371192,
                                    nm: 0.000539956803 // nautical miles, not nanometers
                                },

                                mass: {
                                    g: 1, // use grams as our base
                                    lb: 0.002204622622,
                                    oz: 0.0352739619
                                }
                            };

                            function Unit(type, unit, base) {
                                this.value = base * conversions[type][unit];
                                this.to = {};
                                for (var otherUnit in conversions[type]) {
                                    (function(target) {
                                        this.to[target] = function() {
                                            return new Unit(type, target, base);
                                        }
                                    }).call(this, otherUnit);
                                }
                            }

                            Unit.prototype = {
                                yield: function() {
                                    return this.valueOf();
                                },

                                toString: function() {
                                    return String(this.value);
                                },

                                valueOf: function() {
                                    return this.value;
                                }
                            };

                            var types = {};
                            for (var type in conversions) {
                                (function(type) {
                                    types[type] = function(value) {
                                        var units = {};
                                        for (var unit in conversions[type]) {
                                            (function(unit) {
                                                units[unit] = function() {
                                                    return new Unit(type, unit, value / conversions[type][unit]);
                                                }
                                            }(unit));
                                        }
                                        return units;
                                    };
                                }(type));
                            }

                            return types;
                        }());
                        let windKmh = convert.speed(wind).mph().to.kmh();
                        $('<h4><b>' + description + '</b>' + Math.round(temperature) + ' °C</h4>').appendTo('.page_container-contain .data-temperature');
                        $('<div><h5>Humidité : ' + humidity + ' %</h5><h5>Vent : ' + Math.round(windKmh) + ' km/h</h5></div>').appendTo('.page_container-contain .data-temperature');
                    });

            });

            

            /** SIDEBAR **/
            $('#navbar-icon').click(function() {
                $('body').toggleClass('navbar_open');
            });

            /** POP UP **/
            $('#background_pop_up').click(function() {
                $('body').removeClass('navbar_open');
            });

            /** SCROLL **/
            $(window).scroll(function() {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > 0) {
                    if ($('body').hasClass("scroll_on")) {
                        return false;
                    } else {
                        $('body').addClass('scroll_on');
                    }
                } else {
                    $('body').removeClass('scroll_on');
                }
            });

        });
    </script>
</body>

</html>