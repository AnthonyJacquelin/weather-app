    <!-- FOOTER -->
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="footer_donation">
        <p>Ce site est <b>entièrement gratuit et le restera</b>. Cependant pour payer les frais de serveurs ou la gestion du site nous avons des frais à couvrir.<br/>Si vous avez la possibilité de nous aider, et nous soutenir financièrement, <b>nous vous remercions par avance</b>.</p>
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="VFM6VEQPZJBR8" />
        <input type="image" src="https://i.ibb.co/0c9kDjS/cta-don.png" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
        <img alt="" border="0" src="https://www.paypal.com/en_FR/i/scr/pixel.gif" width="1" height="1" />
    </form>

    <!-- FOOTER -->
    <div id="footer">
        <h5>© Klima 2020</h5>
        <!-- 
        <div>
            <h6>Villes populaires</h6>
            <a href="#">Lacanau</a>
            <a href="#">Paris</a>
            <a href="#">Nice</a>
            <a href="#">Bordeaux</a>
            <a href="#">Carcan</a>
        </div>
        -->
    </div>